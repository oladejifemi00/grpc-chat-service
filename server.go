package main

import (
	"log"
	"net"
	"google.golang.org/grpc"
)


func main() {
	log.Println("Welcome to Go GRPC tutorial!!")
	lis, err:= net.Listen("tcp", ":9000")
	if err!=nil{
		log.Fatal("error::", err)
	}

	grpcServer:=grpc.NewServer()
	err=grpcServer.Serve(lis)
	if err!=nil{
		log.Fatal("error::", err)
	}
}